﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Core.Models;

namespace CMSStarterKit.Controllers.Surface
{
    public class MemberLoginSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {
        [HttpPost]
        [ActionName("MemberLogin")]
        public ActionResult MemberLoginPost(FormCollection form)
        {
            string username = form["uname"];
            string password = form["pCrypt"];
            string email = form["email"];
            int ekpUID = int.Parse(form["UID"]);
            string ekpPWD = form["PWD"];

            if (Membership.ValidateUser(username, password))
            {
                IMember member = Services.MemberService.GetByUsername(username);
                member.IsApproved = true;
                member.SetValue("ekpUID", ekpUID);
                member.SetValue("ekpPassword", ekpPWD);
                Services.MemberService.Save(member);

                FormsAuthentication.SetAuthCookie(username, false);
                return Redirect(string.IsNullOrWhiteSpace(form["redirectUrl"]) ? "/" : form["redirectUrl"]);
            }
            else
            {
                // Check if user exists, but wrong password for some reason
                if (Services.MemberService.Exists(username))
                {
                    TempData["Status"] = "Invalid password";
                    return RedirectToCurrentUmbracoPage();
                }
                else
                {
                    // Username does not exist, so create a new user
                    IMember member = Services.MemberService.CreateMember(username, email, username, "Member");
                    try
                    {
                        member.IsApproved = true;
                        member.SetValue("ekpUID", ekpUID);
                        member.SetValue("ekpPassword", ekpPWD);
                        Services.MemberService.Save(member);
                        Services.MemberService.SavePassword(member, password);
                        Services.MemberService.AssignRole(member.Id, "Basic");
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Unable to create new member " + ex.Message);
                    }

                    FormsAuthentication.SetAuthCookie(username, false);
                    return Redirect(string.IsNullOrWhiteSpace(form["redirectUrl"]) ? "/" : form["redirectUrl"]);
                }
            }
        }
    }
}